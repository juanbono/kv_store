KV Store
=====

Build a KV store that distributes the data across different virtual nodes (genservers in this case) using a hash function to calculate in which virtual node a given pair {key, value} should be stored.

The store should have the following functions:

- put(Key, Value) -> ok.
- get(Key) -> Value | {error, badkey}.
- delete(Key) -> ok | {error, badkey}.
- keys() -> [Key].
- values() -> [Value]

where:
  Value = term()
  Key = term()

Considerations: 
- The number of nodes must be read from a config file.

