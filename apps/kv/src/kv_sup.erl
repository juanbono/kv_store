%%%-------------------------------------------------------------------
%% @doc kv top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(kv_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init([]) ->
    SupFlags = #{strategy => one_for_one, period => 5, intensity => 2},
    NodeSupSpec = #{id => node_sup, start => {kv_node_sup, start_link, []}, type => supervisor},
    KVApiSpec = #{id => kv_api, start => {kv_api, start_link, []}},
    {ok, {SupFlags, [NodeSupSpec, KVApiSpec]}}.


