-module(kv_api).

-export([put/2, get/1, delete/1, keys/0, values/0, nodes/0]).

-export([start_link/0]).

-export([init/1, handle_cast/2, handle_call/3]).

% API
put(Key, Value) ->
    todo.

get(Key) ->
    todo.

delete(Key) ->
    todo.

keys() ->
    todo.

values() ->
    todo.

nodes() ->
    kv_node_sup:get_nodes().

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

% Callbacks
init([]) ->
    {ok, []}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_call(_Msg, _From, State) ->
    {reply, ok, State}.

