-module(kv_node).

-behaviour(gen_server).

-export([start_link/0, put/3, get/2, delete/2]).

-export([init/1, handle_cast/2, handle_call/3]).

% API
start_link() ->
    gen_server:start_link(?MODULE, [], []).

put(Pid, Key, Value) ->
    gen_server:cast(Pid, {put, Key, Value}).

get(Pid, Key) ->
    gen_server:call(Pid, {get, Key}).

delete(Pid, Key) ->
    gen_server:cast(Pid, {delete, Key}).

% Callbacks
init([]) ->
    {ok, maps:new()}.

handle_cast({put, Key, Value}, State) ->
    NewState = maps:put(Key, Value, State),
    {noreply, NewState};

handle_cast({delete, Key}, State) ->
    NewState = safe(fun(Map) -> maps:remove(Key, Map) end, State),
    {noreply, NewState}.

handle_call({get, Key}, _From, State) ->
    Value = safe(fun(Map) -> maps:get(Key, Map) end, State),
    {reply, Value, State}.


safe(Op, Map) ->
    try
        Op(Map)
    catch
        _Class:_Exception:_Stacktrace ->
            {error, badkey}
    end.
