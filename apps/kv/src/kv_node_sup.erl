-module(kv_node_sup).

-behaviour(supervisor).

-export([get_nodes/0]).

-export([start_link/0]).

-export([init/1]).

get_nodes() ->
    [{_Spec, Count}] = supervisor:count_children(),
    Count.

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    SupFlags = #{strategy => one_for_one, period => 3, intensity => 2},
    NumberOfNodes = read_number_of_nodes(),
    Children = [node_spec(Num) || Num <- lists:seq(1, NumberOfNodes)],
    {ok, {SupFlags, Children}}.

node_spec(NodeNumber) ->
  #{id => NodeNumber, start => {kv_node, start_link, []}}.

read_number_of_nodes() ->
    case application:get_env(number_of_nodes) of
        undefined -> 3;
        Value -> Value
    end.
